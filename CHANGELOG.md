# Changelog

## 0.2.1

- Changed the indentation in JSON output to 2.

## 0.2.0

- The items in "items", "oneOf", "anyOf" and "allOf" now get converted as well.

## 0.1.0

- Initialize jsonschema_to_openapi.
